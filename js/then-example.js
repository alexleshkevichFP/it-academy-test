"use strict";

let result = '';

const fetchData = function (name) {

     return fetch(`https://fe.it-academy.by/Examples/words_tree/${name}`)
         .then(function (res) {
             return res.text();
         })
         .then(async function (data) {
             let parsedData = null;

             try {
                 parsedData = JSON.parse(data);

                 for (let file of parsedData) {
                     try {
                         await fetchData(file);
                     } catch (err) {
                         console.log('any err handler');
                     }
                 }
             } catch (err) {
                 result += `${data} `;
                 return Promise.resolve(result);
             }

         })
         .then(function (data) {
             return result;
         })
}

fetchData('root.txt')
    .then(alert)
