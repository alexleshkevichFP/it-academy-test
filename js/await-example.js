"use strict";

let result = '';

const fetchData = async (name) => {
    const response = await fetch(`https://fe.it-academy.by/Examples/words_tree/${name}`);

    if (!response.ok) {
        throw new Error("Not 2xx response");
    }

    const data = await response.text();
    let parsedData = null;

    try {
        parsedData = JSON.parse(data);

        for (let file of parsedData) {
            try {
                await fetchData(file);
            } catch (err) {
                console.log('any err handler');
            }
        }
    } catch (err) {
        result += `${data} `;
    }

    return result;
};

fetchData('root.txt').then(alert);

